﻿using EbayXunitAutomation.Pages.Common;
using System.Web;
using OpenQA.Selenium;
using System.Text;
using System.Net.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EbayXunitAutomation.Pages
{
    public class SearchPage : BasePage
    {
        public SearchPage(IWebDriver driver)
        {
            this.driver = driver;
            Loaded = driver.WaitUntilElementIsPresent(By.Id("gh-ac"), 30);
        }

        public void SelectBrand(string brandName)
        {
            var brandSearchNameId = "w3-w0-w2-w2-0[0]";
            driver.WaitUntilElementIsPresent(By.Id(brandSearchNameId), 30);
            driver.FindElement(By.Id(brandSearchNameId)).SendKeys(brandName + Keys.Tab);
            string brandXpathFormat = "//a[contains(@href,'Brand={0}')]/input[@type='checkbox']";
            var brandXpath = string.Format(brandXpathFormat, brandName);
            driver.WaitUntilElementIsPresent(By.XPath(brandXpath), 30);
            ForceWait(2);
            driver.FindElement(By.XPath(brandXpath)).Click();
        }

        public void SelectSize(string size)
        {
            string sizeXpathFormat = "//a[contains(@href,'={0}')]/input[@type='checkbox'][@aria-label='{1}']";
            var sizeEncode = size.Replace(".", "%252E");
            var sizeXpath = string.Format(sizeXpathFormat, sizeEncode, size);
            driver.WaitUntilElementIsPresent(By.XPath(sizeXpath), 30);
            driver.FindElement(By.XPath(sizeXpath)).Click();
        }

        public void OrderBy(string orderByName)
        {
            var sortXpath = "//*[@id='srp-river-results-SEARCH_STATUS_MODEL_V2-w0-w1_btn']/div";
            driver.WaitUntilElementIsPresent(By.XPath(sortXpath), 30);
            var act = new OpenQA.Selenium.Interactions.Actions(driver);
            var element = driver.FindElement(By.XPath(sortXpath));
            act.MoveToElement(element);
            act.SendKeys(element, Keys.Space);
            act.Perform();

            var sortNameXpathFormat = "//div[@class='srp-sort']/ul[@class='srp-sort__menu']/li/a/span[contains(text(),'{0}')]/..";
            var sortNameXpath = string.Format(sortNameXpathFormat, orderByName);
            driver.WaitUntilElementIsPresent(By.XPath(sortNameXpath), 30);
            element = driver.FindElement(By.XPath(sortNameXpath));
            act.MoveToElement(element);
            act.SendKeys(element, Keys.Space);
            act.Perform();
        }

        public string GetItem(int index)
        {
            var mainXpathFormat = "//*[@id='srp-river-results']/ul/li[{0}]/div/div[@class='s-item__info clearfix']";
            var mainXpath = string.Format(mainXpathFormat, index);

            var titleXpathFormat = mainXpath + "/a/h3[@class='s-item__title']";
            var titleXpath = string.Format(titleXpathFormat, index);
            driver.WaitUntilElementIsPresent(By.XPath(titleXpath), 30);
            var tittleElem = driver.FindElement(By.XPath(titleXpath));
            var title = tittleElem.Text;

            var priceXpathFormat = mainXpath + "/div[@class='s-item__details clearfix']/div/span[@class='s-item__price']";
            var priceXpath = string.Format(priceXpathFormat, index);
            driver.WaitUntilElementIsPresent(By.XPath(priceXpath), 30);
            var priceElem = driver.FindElement(By.XPath(priceXpath));
            var price = priceElem.Text;

            var logisticsCostXpathFormat = mainXpath + "/div[@class='s-item__details clearfix']/div/span[@class='s-item__shipping s-item__logisticsCost']";
            var logisticsCostXpath = string.Format(logisticsCostXpathFormat, index);
            driver.WaitUntilElementIsPresent(By.XPath(logisticsCostXpath), 30);
            var logisticsCostElem = driver.FindElement(By.XPath(logisticsCostXpath));
            var logisticsCost = logisticsCostElem.Text;

            var totalformat = "[Item: {0}] - [Price: {1}] [Shipping: {2}]";
            var total = string.Format(totalformat, title, price, logisticsCost);

            return total;
        }

        public string GetTitle(int index)
        {
            var mainXpathFormat = "//*[@id='srp-river-results']/ul/li[{0}]/div/div[@class='s-item__info clearfix']";
            var mainXpath = string.Format(mainXpathFormat, index);

            var titleXpathFormat = mainXpath + "/a/h3[@class='s-item__title']";
            var titleXpath = string.Format(titleXpathFormat, index);
            driver.WaitUntilElementIsPresent(By.XPath(titleXpath), 30);
            var tittleElem = driver.FindElement(By.XPath(titleXpath));
            var title = tittleElem.Text;

            var totalformat = "[Item: {0}]";
            var total = string.Format(totalformat, title);

            return total;
        }

        public string GetPriceShipping(int index)
        {
            var mainXpathFormat = "//*[@id='srp-river-results']/ul/li[{0}]/div/div[@class='s-item__info clearfix']";
            var mainXpath = string.Format(mainXpathFormat, index);

            var priceXpathFormat = mainXpath + "/div[@class='s-item__details clearfix']/div/span[@class='s-item__price']";
            var priceXpath = string.Format(priceXpathFormat, index);
            driver.WaitUntilElementIsPresent(By.XPath(priceXpath), 30);
            var priceElem = driver.FindElement(By.XPath(priceXpath));
            var price = priceElem.Text;

            var logisticsCostXpathFormat = mainXpath + "/div[@class='s-item__details clearfix']/div/span[@class='s-item__shipping s-item__logisticsCost']";
            var logisticsCostXpath = string.Format(logisticsCostXpathFormat, index);
            driver.WaitUntilElementIsPresent(By.XPath(logisticsCostXpath), 30);
            var logisticsCostElem = driver.FindElement(By.XPath(logisticsCostXpath));
            var logisticsCost = logisticsCostElem.Text;

            var totalformat = "[Price: {0}] [Shipping: {1}]";
            var total = string.Format(totalformat, price, logisticsCost);

            return total;
        }

        public List<string> GetListOfItems(int range)
        {
            var itemList = new List<string>();
            for (int idx = 1; idx <= range; idx++)
            {
                itemList.Add(GetItem(idx));
            }
            return itemList;
        }

        public List<string> GetListOfPriceShipping(int range)
        {
            var itemList = new List<string>();
            for (int idx = 1; idx <= range; idx++)
            {
                itemList.Add(GetPriceShipping(idx));
            }
            return itemList;
        }

        public List<string> GetListOfPriceShipping_OrderByPrice(int range, string currency, string freeShippingMessage)
        {
            var itemList = new List<string>();
            for (int idx = 1; idx <= range; idx++)
            {
                itemList.Add(GetPriceShipping(idx));
            }

            var itemDoubleList = new List<double>();

            foreach (var item in itemList)
            {
                double total;
                var priceWithCurrency = item.Replace("[Price: ", "").Trim();
                priceWithCurrency = priceWithCurrency.Substring(0, priceWithCurrency.IndexOf(']')).Trim();
                var priceWithNoCurrency = priceWithCurrency.Replace(currency, "").Trim();
                var price = Convert.ToDouble(priceWithNoCurrency);

                var logCostWithCurrency = item.Substring(item.IndexOf("[Shipping: "));
                logCostWithCurrency = logCostWithCurrency.Replace("[Shipping: ", "");
                logCostWithCurrency = logCostWithCurrency.Substring(0, logCostWithCurrency.IndexOf(']'));
                if (!logCostWithCurrency.Contains(freeShippingMessage))
                {
                    logCostWithCurrency = logCostWithCurrency.Replace("+", "").Trim();
                    logCostWithCurrency = logCostWithCurrency.Replace(currency, "").Trim();
                    logCostWithCurrency = logCostWithCurrency.Substring(0, logCostWithCurrency.IndexOf(' ')).Trim();
                    var logCostWithNoCurrency = logCostWithCurrency.Replace(currency, "").Trim();
                    var logCost = Convert.ToDouble(logCostWithNoCurrency);
                    total = price + logCost;
                }
                else
                    total = price;

                itemDoubleList.Add(total);
            }

            itemDoubleList = itemDoubleList.OrderBy(item => item).ToList();

            itemList = new List<string>();

            foreach (var item in itemDoubleList)
            {
                itemList.Add(item.ToString());
            }

            return itemList;
        }
    }
}
