﻿using OpenQA.Selenium;
using System.Threading;

namespace EbayXunitAutomation.Pages.Common
{
    public class BasePage
    {
        public IWebDriver driver { get; set; }
        public bool Loaded { get; set; }
        public string PageName { get; set; }

        const string pageLoaded = "Page loaded";
        const string pageDidNotLoad = "Page did not load";

        public void Continue()
        {
            driver.WaitUntilElementIsNotPresent(By.Id("fadeCurtain"), 10000);
            driver.FindElement(By.Id("btnContinue")).Click();
            WaitForLoading();
        }

        public bool IsLoaded()
        {
            return Loaded;
        }

        public string GetPageLoadedStatus()
        {
            if (IsLoaded())
                return LoadedStatus;
            else
                return DidNotLoadStatus;
        }

        public string LoadedStatus
        {
            get
            {
                return string.Format("{0} {1}", PageName, pageLoaded);
            }
        }

        public string DidNotLoadStatus
        {
            get
            {
                return string.Format("{0} {1}", PageName, pageDidNotLoad);
            }
        }


        public void WaitForLoading()
        {
            driver.WaitUntilElementIsNotPresent(By.Id("modalLoading"), 20);
        }

        public void ForceWait(int seconds)
        {
            int milliseconds = seconds * 1000;
            Thread.Sleep(milliseconds);
        }
    }
}
