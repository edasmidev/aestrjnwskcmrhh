﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace EbayXunitAutomation.Pages.Common
{
    public static class WebUtils
    {
        public static void WaitUntilElementIsNotPresent(this IWebDriver driver, By by, int seconds)
        {
            int currentMilliseconds = seconds * 1000;
            bool exists = true;
            while (exists && currentMilliseconds > 0)
            {
                var count = driver.FindElements(by).Count;
                if (count == 0)
                    exists = false;
                else if (count == 1)
                {
                    try
                    {
                        var element = driver.FindElement(by);
                        exists = element.Displayed;
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("Unable to locate element"))
                            exists = false;
                    }
                }

                if (exists)
                {
                    currentMilliseconds = currentMilliseconds - 500;
                    Thread.Sleep(500);
                }
            }
        }

        public static bool WaitUntilElementIsPresent(this IWebDriver driver, By by, int seconds)
        {
            int currentMilliseconds = seconds * 1000;
            bool exists = false;
            while (!exists && currentMilliseconds > 0)
            {
                var count = driver.FindElements(by).Count;
                if (count == 1)
                {
                    try
                    {
                        var element = driver.FindElement(by);
                        exists = element.Displayed;
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("element is not attached to the page document") || ex.Message.Contains("Unable to locate element"))
                            exists = false;
                    }
                }

                if (!exists)
                {
                    currentMilliseconds = currentMilliseconds - 500;
                    Thread.Sleep(500);
                }
            }
            return exists;
        }
    }
}
