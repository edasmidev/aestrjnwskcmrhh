﻿using EbayXunitAutomation.Pages.Common;
using OpenQA.Selenium;

namespace EbayXunitAutomation.Pages
{
    public class DashboardPage : BasePage
    {
        public DashboardPage(IWebDriver driver)
        {
            this.driver = driver;
            Loaded = driver.WaitUntilElementIsPresent(By.Id("gh-ac"), 30);
        }

        public SearchPage SearchItem(string itemName)
        {
            driver.WaitUntilElementIsPresent(By.Id("gh-ac"), 30);
            driver.FindElement(By.Id("gh-ac")).SendKeys(itemName);
            driver.WaitUntilElementIsPresent(By.Id("gh-btn"), 30);
            driver.FindElement(By.Id("gh-btn")).Click();
            return new SearchPage(driver);
        }
    }
}
