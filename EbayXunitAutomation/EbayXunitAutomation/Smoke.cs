﻿using EbayXunitAutomation.Pages;
using NUnit.Framework;
using EbayXunitAutomation.Test;

namespace EbayXunitAutomation
{
    public class Smoke : EbayAutomationTest
    {
        public Smoke() { }

        [Test]
        public void EbayShop()
        {
            driver.Navigate().GoToUrl("https://www.ebay.com");
            var dashboardPage = new DashboardPage(driver);
            var searchPage = dashboardPage.SearchItem("shoes");
            searchPage.SelectBrand("PUMA");
            searchPage.SelectSize("10");
            searchPage.OrderBy("Precio + Envío: más bajo primero");

            PrintConsoleList(searchPage.GetListOfItems(5));
            TakeScreenshot();
            PrintConsoleList(searchPage.GetListOfPriceShipping_OrderByPrice(5, "S/.", "Envío gratis"));
            TakeScreenshot();
        }
    }
}
