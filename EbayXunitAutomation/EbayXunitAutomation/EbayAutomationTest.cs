﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EbayXunitAutomation.Test
{
    public class EbayAutomationTest
    {

        public IWebDriver driver { get; set; }
        public EbayAutomationTest() { }

        [SetUp]
        public void Init()
        {
            driver = new ChromeDriver();
        }

        [TearDown]
        public void CleanUp()
        {
            driver.Quit();
        }

        public void PrintConsoleList(List<string> customList)
        {
            foreach (var item in customList)
            {
                Console.WriteLine(item);
            }
        }

        public List<string> ReorderListByName(List<string> customList)
        {
            return customList.OrderBy(item => item).ToList();
        }

        public void TakeScreenshot()
        {
            var filename = Directory.GetCurrentDirectory() + DateTime.Now.ToString("yyyyMMddHHmmssfffff") + ".png";
            // Take screenshot
            Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
            // Create directory if doesn't exist
            Directory.CreateDirectory(Path.GetDirectoryName(filename));
            // Save file            
            ss.SaveAsFile(filename);

            var currentContext = TestContext.CurrentContext;
            if (currentContext.Result.Outcome != ResultState.Success)
            {
                Console.WriteLine("filename: " + filename);
            }
        }
    }
}
